{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-missing-methods #-}

module Servant.Client.TypeScript
  ( -- * Generator functions
    gen
  , tsClient
    -- * Named Route Combinator
  , TSRouteName
    -- * Type Classes
  , Fletch (..)
  , GenAll (..)
    -- * AST data types
  , DocType (..)
  , HTTPorWS (..)
  , InputMethod (..)
  , ReturnType (..)
  , TSDef
  , TSInstance (..)
  , URIBit (..)
  ) where

import           Data.Aeson (ToJSON (toJSON))
import qualified Data.Aeson as Aeson
import           Data.Aeson.TypeScript
  ( TSConstructName (..)
  , TSDefinition (typeDefinition)
  )
import           Data.Containers.ListUtils (nubOrd)
import           Data.Kind (Constraint, Type)
import           Data.List (sort)
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Maybe (fromMaybe, mapMaybe)
import           Data.String (fromString)
import           Data.Text as T
  ( Text
  , filter
  , intercalate
  , map
  , pack
  , replace
  , unpack
  )
import           Data.Text.Encoding (decodeUtf8')
import           Data.Typeable
  ( Proxy (..)
  , Typeable
  , eqT
  , type (:~:) (Refl)
  , typeRep
  )
import           GHC.TypeLits
  ( ErrorMessage (Text)
  , KnownSymbol
  , Symbol
  , TypeError
  , symbolVal
  )
import           Network.HTTP.Types (Method, urlEncode)
import           Servant (HasServer (..))
import           Servant.API as Servant
  ( Capture'
  , CaptureAll
  , Description
  , EmptyAPI
  , Fragment
  , HList (HCons)
  , Header'
  , Headers (Headers)
  , JSON
  , NoContent
  , NoContentVerb
  , QueryFlag
  , QueryParam'
  , QueryParams
  , ReflectMethod (reflectMethod)
  , ReqBody'
  , ResponseHeader (..)
  , Summary
  , Verb
  , type (:>)
  , (:<|>)
  )
import qualified Servant.API.WebSocket as Ws
import           Servant.API.WebSocket (TypedWebSocket)
import           Servant.API.WebSocketConduit
  ( MessageType (..)
  , WebSocketConduitRaw
  )
import           Servant.Client (HasClient (..))
import qualified Servant.Multipart as MP


hush :: Either a b -> Maybe b
hush (Right x) = Just x
hush _         = Nothing


headMay :: [a] -> Maybe a
headMay (x:_) = Just x
headMay []    = Nothing


-- | What is the means of input for this @URIBit@?
type InputMethod :: Type
data InputMethod
  = Capture
  | Query
  | Querys
  | Header_
  | Body
  | Mulipart
  | Fragment
  | WSInput
  deriving stock (Eq, Show)


-- | What kind of API documentation are we using for this route?
type DocType :: Type
data DocType
  = Summary'
  | Description'
  | TSRouteName'
  deriving stock (Show)


-- | Combinator to name the API call in TypeScript
type TSRouteName :: Symbol -> Type
data TSRouteName (sym :: Symbol)


instance HasServer rest ctx => HasServer (TSRouteName sym :> rest) ctx where
  type ServerT (TSRouteName sym :> rest) m = ServerT rest m
  route _ = route (Proxy @rest)
  hoistServerWithContext _ _ = hoistServerWithContext (Proxy @rest) (Proxy @ctx)

instance HasClient m rest => HasClient m (TSRouteName sym :> rest) where
  type Client m (TSRouteName sym :> rest) = Client m rest
  clientWithRoute _ _ = clientWithRoute (Proxy @m) (Proxy @rest)
  hoistClientMonad _ _ = hoistClientMonad (Proxy @m) (Proxy @rest)


-- | An input chunk of the URI
type URIBit :: Type
data URIBit = PathBit Text
            | ArgBit InputMethod Text Text
            | DocBit DocType Text
  deriving stock (Show)


type HTTPorWS :: Type
data HTTPorWS = HTTP
              | WS MessageType


type ReturnType :: Type
data ReturnType = ReturnType
  { rtMethod     :: Method
  , rtStatusCode :: Int
  , rtParsedType :: Text
  }


-- | Type class that iterates over the servant type and seperates out inputs and outputs
type Fletch :: Type -> Constraint
class Fletch route where
  argBits :: [URIBit]
  returnType :: ReturnType
  protocol :: HTTPorWS

instance (KnownSymbol s, Fletch xs) => Fletch (TSRouteName s :> xs) where
  argBits = DocBit TSRouteName' (pack . symbolVal $ Proxy @s) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (KnownSymbol s, Fletch xs) => Fletch (Summary s :> xs) where
  argBits = DocBit Summary' (pack . symbolVal $ Proxy @s) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (KnownSymbol s, Fletch xs) => Fletch (Description s :> xs) where
  argBits = DocBit Description' (pack . symbolVal $ Proxy @s) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (TypeError ('Text "💠 EmptyAPI's cannot be Fletched as they do not make a request")) => Fletch EmptyAPI

instance (TypeError ('Text "💠 EmptyAPI's cannot be GenAll as they do not make a request")) => GenAll EmptyAPI

instance (TSInstance i, TSInstance o, Typeable mt) => Fletch (WebSocketConduitRaw msec mt i o) where
  argBits = [ArgBit WSInput "WebSocketInput" $ typeInstance (Proxy @i)]
  returnType = ReturnType
    { rtMethod = "connect"
    , rtStatusCode = 200
    , rtParsedType = typeInstance (Proxy @o)
    }
  protocol = WS case eqT @mt @'JSONMessage of
     Just Refl -> JSONMessage
     Nothing -> case eqT @mt @'BinaryMessage of
       Just Refl -> BinaryMessage
       Nothing   -> error "Unhandled Message Type should be impossible"

instance (TSInstance i, TSInstance o, Typeable mt) => Fletch (TypedWebSocket mt i o) where
  argBits = [ArgBit WSInput "WebSocketInput" $ typeInstance (Proxy @i)]
  returnType = ReturnType
    { rtMethod = "connect"
    , rtStatusCode = 200
    , rtParsedType = typeInstance (Proxy @o)
    }
  protocol = WS case eqT @mt @'Ws.Text of
     Just Refl -> JSONMessage
     Nothing -> case eqT @mt @'Ws.Binary of
       Just Refl -> BinaryMessage
       Nothing   -> error "Unhandled Message Type should be impossible"

instance (Fletch xs, KnownSymbol s) => Fletch ((s :: Symbol) :> xs) where
  argBits = PathBit (pack $ symbolVal (Proxy @s)) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance {-# OVERLAPPING #-} (TypeError ('Text "💠 200 with JSON indicates JSON parseability, but NoContent is unparsable; consider using 204 or 205 instead")) => Fletch (Verb method 200 (JSON ': _ms) NoContent)

instance (TSInstance x, ReflectMethod method)
  => Fletch (Verb method 200 (JSON ': _ms) x) where
  argBits = []
  returnType = ReturnType
    { rtMethod     = reflectMethod $ Proxy @method
    , rtStatusCode = 200
    , rtParsedType = typeInstance (Proxy @x)
    }
  protocol = HTTP

instance {-# OVERLAPPING #-} (ReflectMethod method) => Fletch (Verb method 204 _ms NoContent) where
  argBits = argBits @(NoContentVerb method)
  returnType = returnType @(NoContentVerb method)
  protocol = protocol @(NoContentVerb method)

instance (TSInstance x, ReflectMethod method) => Fletch (Verb method 201 _ms x) where
  argBits = []
  returnType = ReturnType
    { rtMethod     = reflectMethod $ Proxy @method
    , rtStatusCode = 201
    , rtParsedType = typeInstance (Proxy @x)
    }
  protocol = protocol @(NoContentVerb method)

instance (ReflectMethod method) => Fletch (Verb method 205 _ms NoContent) where
  argBits = argBits @(NoContentVerb method)
  returnType = ReturnType
    { rtMethod     = reflectMethod $ Proxy @method
    , rtStatusCode = 205
    , rtParsedType = "null"
    }
  protocol = protocol @(NoContentVerb method)

instance (ReflectMethod method) => Fletch (NoContentVerb method) where
  argBits = []
  returnType = ReturnType
    { rtMethod     = reflectMethod $ Proxy @method
    , rtStatusCode = 204
    , rtParsedType = "null"
    }
  protocol = HTTP

instance (Fletch xs, KnownSymbol doc, TSInstance arg) => Fletch (Capture' _ys doc arg :> xs) where
  argBits = ArgBit Capture (pack $ symbolVal (Proxy @doc)) (typeInstance (Proxy @arg)) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, KnownSymbol doc, TSInstance arg) => Fletch (CaptureAll doc arg :> xs) where
  argBits = ArgBit Capture (pack $ symbolVal (Proxy @doc)) (typeInstance (Proxy @arg)) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, KnownSymbol doc, TSInstance arg) => Fletch (QueryParam' _ys doc arg :> xs) where
  argBits = ArgBit Query (pack $ symbolVal (Proxy @doc)) (typeInstance (Proxy @arg)) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, KnownSymbol doc, TSInstance [arg]) => Fletch (QueryParams doc arg :> xs) where
  argBits = ArgBit Querys (pack $ symbolVal (Proxy @doc)) (typeInstance (Proxy @[arg])) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, KnownSymbol doc) => Fletch (QueryFlag doc :> xs) where
  argBits = ArgBit Query (pack $ symbolVal (Proxy @doc)) (typeInstance (Proxy @Bool)) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, KnownSymbol doc, TSInstance arg) => Fletch (Header' _ys doc arg :> xs) where
  argBits = ArgBit Header_ (pack $ symbolVal (Proxy @doc)) (typeInstance (Proxy @arg)) : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, TSInstance x, Typeable x) => Fletch (ReqBody' _ys '[JSON] x :> xs) where
  argBits = ArgBit Body (toArgName @x) name : argBits @xs
    where name = typeInstance (Proxy @x)
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs, TSInstance x, Typeable x) => Fletch (Fragment x :> xs) where
  argBits = ArgBit Fragment (toArgName @x) name : argBits @xs
    where name = typeInstance (Proxy @x)
  returnType = returnType @xs
  protocol = protocol @xs

instance (Fletch xs) => Fletch (MP.MultipartForm tag x :> xs) where
  argBits = ArgBit Mulipart "formData" "FormData" : argBits @xs
  returnType = returnType @xs
  protocol = protocol @xs


encodeJSVar :: Text -> Text
encodeJSVar = T.filter (\x -> x /= '<' && x /= '>') . T.map \case
  ' ' -> us
  '-' -> us
  x -> x
  where us = '_'


uriKey :: forall api. Fletch api => Text
uriKey = fromMaybe (mconcat
  [ "/"
  , T.intercalate "/" $ marge @api \case
          PathBit s -> Just s
          ArgBit Capture doc _ -> mappend ":" <$> urlEncode' doc
          _ -> Nothing
  , let queries = marge @api \case
          ArgBit Query doc _ -> urlEncode' doc
          ArgBit Querys doc _ -> urlEncode' doc
          _ -> Nothing
     in if null queries then mempty else "?" <> T.intercalate "&" queries
  , let req = marge @api \case
          ArgBit Body doc _ -> Just doc
          ArgBit Mulipart doc _ -> Just doc
          _ -> Nothing
     in if null req then mempty else "(" <> mconcat req <> ")"
  , let hs = marge @api \case
          ArgBit Header_ doc _ -> Just doc
          _ -> Nothing
    in if null hs then mempty else "{" <> T.intercalate "," hs <> "}"
  , let frag = marge @api \case
          ArgBit Fragment _ _ -> Just ()
          _ -> Nothing
     in if null frag then mempty else "#fragment"
  ]) . headMay $ marge @api \case
     DocBit TSRouteName' name -> Just name
     _ -> Nothing


docs :: forall api. Fletch api => Text
docs = mconcat $ marge @api \case
      DocBit Summary' s -> Just $ pack ['/', '/', ' '] <> s <> "\n  "
      DocBit Description' d -> Just $ "/*\n   * " <> d <> "\n   */\n  "
      _ -> Nothing


urlEncode' :: Text -> Maybe Text
urlEncode' = hush . decodeUtf8' . urlEncode True . fromString . unpack


genWebSocket :: forall api. Fletch api => Text
genWebSocket = T.intercalate "\n"
  [ docs @api <> "\"" <> uriKey @api <> "\": (" <> functionArgs @api <> "):"
  , "    Promise<{ send : (input: " <> wsInput <> ") => void"
  , "            , receive : (cb: (output: " <> wsOutput <> ") => void) => void"
  , "            , raw : WebSocket"
  , "    }> => {"
  , "      if(!API.baseWS){"
  , "        const pr = window.location.protocol === \"http:\" ? \"ws:\" : \"wss:\";"
  , "        API.baseWS = `${pr}//${window.location.host}`;"
  , "        console.info(`You have not set API.baseWS, so it has been defaulted to ${API.baseWS}."
  , "        Please note, that WebSocket's need to have absolute uri's including protocol.`);"
  , "      }"
  , "      const ws = new WebSocket(`${API.baseWS}" <> pathWithCaptureArgs @api <> queryParams @api <> "`" <> headersWS @api <> ");"
  , "      return Promise.resolve({"
  , "        send: (input: " <> wsInput <> ") => ws.send(JSON.stringify(input)),"
  , "        receive: (cb: ((output: " <> wsOutput <> ") => void)) =>"
  , "          ws.onmessage = " <> onmessage <> ","
  , "        raw: ws"
  , "      });"
  , "  }"
  ]
  where
    wsInput = case Prelude.filter (\case
      ArgBit WSInput _ _ -> True
      _ -> False) $ argBits @api of
      [ArgBit WSInput _ x] -> x
      x                    -> error $ "Bad argits: " <> show x <> " This is a hack to transfer the input of the WebSocket to the function here."
    (wsOutput, onmessage) = case protocol @api of
      WS BinaryMessage ->
        ( "ArrayBuffer"
        , "(message) => cb(message.data)" :: Text
        )
      WS JSONMessage   ->
        ( rtParsedType $ returnType @api
        , "(message: MessageEvent<string>) => cb(JSON.parse(message.data) as " <> wsOutput <> ")"
        )
      HTTP             -> error "Impossible, HTTP is not usable in genWebSocket"


genHTTP :: forall api. Fletch api => Text
genHTTP = intercalate "\n"
  [ docs @api <> "\"" <> uriKey @api <> "\": (() => {"
  , "  const urlBuilder = (" <> uriArgs @api <> ") => `${API.base}" <> pathWithCaptureArgs @api <> queryParams @api <> "`;"
  , "  const f = async (" <> functionArgs @api <> "): Promise<" <> rtParsedType <> "> => {"
  , "    const uri = urlBuilder(" <> uriCallArgs @api <> ");"
  , "    return fetch(uri, {"
  , "      method: " <> pack (show rtMethod) <> headers @api <> reqBody @api <> multiBody @api <> ","
  , "      redirect: 'manual'"
  , "    }).then(res => {"
  , "      const location = res.headers.get('Location');"
  , "      if (res.status === 401 && location) {"
  , "        window.location.replace(location);"
  , "        return Promise.reject(res);"
  , "      } else {"
  , "        return res.status === " <> pack (show rtStatusCode)
  , "          ? " <> yorick
  , "          : Promise.reject(res);"
  , "      }"
  , "    });"
  , "  };"
  , "  f.urlBuilder = urlBuilder;"
  , "  return f; })()"
  ]
  where ReturnType {..} = returnType @api
        yorick | rtStatusCode == 204 = "Promise.resolve(null)" :: Text
               | rtStatusCode == 205 = "Promise.resolve(null)"
               | otherwise           = "(res.json() as Promise<" <> rtParsedType <> ">)"


queryParams :: forall api. Fletch api => Text
queryParams = (\x -> if x == "" then x else "?" <> x) . intercalate "&" $ marge @api \case
  ArgBit Query doc _ -> do
    doc' <- urlEncode' doc
    let var = encodeJSVar doc
    pure $ "${" <> var <> " === null || " <> var <> " === undefined ? \"\" : `" <> doc' <> "=${" <> var <> "}`}"
  ArgBit Querys doc _ -> do
    doc' <- urlEncode' doc
    let var = encodeJSVar doc
    pure $ "${" <> var <> ".reduceRight((acc,x) => x === null || x === undefined ? \"\" : \"" <> doc' <> "=\" + x + (acc ? \"&\" + acc : \"\"), \"\")}"
  _ -> Nothing


pathWithCaptureArgs :: forall api. Fletch api => Text
pathWithCaptureArgs = mappend "/" . intercalate "/" $ marge @api \case
  PathBit s -> Just s
  ArgBit Capture doc _ -> Just $ "${" <> encodeJSVar doc <> "}"
  _ -> Nothing


headers :: forall api. Fletch api => Text
headers = let
  hs = intercalate ",\n        " $ marge @api \case
    ArgBit Header_ doc "string" -> Just $ "\"" <> doc <> "\": " <> encodeJSVar doc
    ArgBit Header_ doc _ -> Just $ "\"" <> doc <> "\": \"\" + " <> encodeJSVar doc
    ArgBit Body _ _ -> Just "'Content-Type': 'application/json'"
    _ -> Nothing
  in if hs == mempty then "" else intercalate "\n" [ ","
                                                   , "      headers: {"
                                                   , "        " <> hs
                                                   , "      }"
                                                   ]


headersWS :: forall api. Fletch api => Text
headersWS = let
  hs = intercalate ", " $ marge @api \case
    ArgBit Header_ doc "string" -> Just $ encodeJSVar doc
    ArgBit Header_ doc _ -> Just $ "\"\" + " <> encodeJSVar doc
    _ -> Nothing
  in if hs == mempty then ("" :: Text) else ", [" <> hs <> "]"


reqBody :: forall api. Fletch api => Text
reqBody = case
  marge @api \case
    ArgBit Body doc _ -> Just $ encodeJSVar doc
    _ -> Nothing
  of [doc] -> intercalate "\n" [","
                               , "      body: JSON.stringify(" <> doc <> ")"
                               ]
     _ -> ("" :: Text)


multiBody :: forall api. Fletch api => Text
multiBody = case
  marge @api \case
    ArgBit Mulipart doc _ -> Just $ encodeJSVar doc
    _ -> Nothing
  of [doc] -> intercalate "\n" [","
                               , "      body: " <> doc
                               ]
     _ -> ("" :: Text)


functionArgs :: forall api. Fletch api => Text
functionArgs = intercalate "," $ marge @api \case
  ArgBit y doc x
    -- Fragments are not captured by a servant server, useful with Link
    | y /= Fragment
    -- WSInput is used to in .send, and not required as initial arguments
    && y /= WSInput -> Just $ encodeJSVar doc <> ":" <> x
  _ -> Nothing


uriCallArgs :: forall api. Fletch api => Text
uriCallArgs = intercalate "," $ marge @api \case
  ArgBit y doc _
    | y == Capture || y == Query || y == Querys
    -> Just $ encodeJSVar doc
  _ -> Nothing


uriArgs :: forall api. Fletch api => Text
uriArgs = intercalate "," $ marge @api \case
  ArgBit y doc x
    | y == Capture || y == Query || y == Querys
    -> Just $ encodeJSVar doc <> ":" <> x
  _ -> Nothing


marge :: forall api b. Fletch api => (URIBit -> Maybe b) -> [b]
marge = flip mapMaybe $ argBits @api


-- | Obtain the String for the client a la carte without type definitions
gen :: forall (api :: Type).
  ( GenAll api
  ) => Text
gen = intercalate "\n" ["export const API = {"
                       , "  base: \"\","
                       , "  baseWS: \"\","
                       , "  " <> generations
                       , "};" ] where generations = genAll @api


-- | The type class for iterating over the API type
type GenAll :: Type -> Constraint
class GenAll a where
  genAll :: Text

-- | Handle left association of routes (IE parens on the left)
instance {-# OVERLAPS #-} (GenAll (route :<|> subrest), GenAll rest) => GenAll ((route :<|> subrest) :<|> rest) where
  genAll = genAll @(route :<|> subrest) <> ",\n" <> genAll @rest

-- | Handle right association of routes (IE parens on the left)
instance (Fletch route, GenAll rest) => GenAll (route :<|> rest) where
  genAll = genAll @route <> ",\n" <> genAll @rest

-- | Handle right association of routes (IE parens on the left)
instance {-# OVERLAPPABLE #-} Fletch route => GenAll route where
  genAll = case protocol @route of
    WS _ -> genWebSocket @route
    HTTP -> genHTTP @route


type TSDef :: k -> Type
data TSDef a


type TypeDecls :: [Type] -> Constraint
class TypeDecls xs where typeDecls :: [Text]
instance (TypeDecls xs, TSDefinition x) => TypeDecls (TSDef x ': xs) where
  typeDecls = typeDefinition (Proxy @x) : typeDecls @xs
instance TypeDecls '[] where
  typeDecls = []


-- | Generate complete TypeScript client for a given api
tsClient :: forall xs api. (TypeDecls xs, GenAll api) => Text
tsClient = intercalate "\n" (sort . nubOrd $ typeDecls @xs)
  <> "\n" <> gen @api


type FromHList :: [Type] -> Constraint
class FromHList hs where
  fromHList :: HList hs -> Map.Map Text Aeson.Value

instance (KnownSymbol s, ToJSON h, FromHList hs) => FromHList (Header' ls s h ': hs) where
  fromHList (HCons (Servant.Header a) xs) = Map.singleton (pack . symbolVal $ Proxy @s) (toJSON a) <> fromHList xs
  fromHList (HCons MissingHeader xs) = fromHList xs
  fromHList (HCons (UndecodableHeader _) xs) = fromHList xs

instance FromHList '[] where
  fromHList = mempty

instance (Aeson.ToJSON x, FromHList xs) => Aeson.ToJSON (Headers xs x) where
  toJSON (Headers x xs) = Aeson.object
    [ "content" Aeson..= x
    , "headers" Aeson..= fromHList xs
    ]


instance TSConstructName NoContent where
  typeConstruct _ _ = "null"


toArgName :: forall a. (Typeable a) => Text
toArgName = encodeJSVar
  . replace "(" "_"
  . replace ")" "_"
  . replace ")(" "_"
  . replace " " ""
  . replace "[Char]" "String"
  . pack . show $ typeRep (Proxy @a)


-- | The full concrete type name.
-- This is not Poly Kinded but statically kinded to @Type@
-- as we must demand that there by no outstanding type variables.
-- We need all of them to be concrete to determine the type name.
-- In theory there is a funky superclass relationship with @TSConstructName@.
-- This class also needs to handle primitives like @TSConstructName@, and so
-- may not have a corresponding @TSDefinition@.
type TSInstance :: Type -> Constraint
class TSInstance a where
  -- | Fully concrete version of typeConstruct
  typeInstance :: Proxy a -> Text
  default typeInstance :: TSConstructName a => Proxy a -> Text
  typeInstance = flip typeConstruct []


type TSNamedInstance :: Type -> Constraint
type TSNamedInstance a = (TSInstance a, TSConstructName a)

instance TSConstructName (Map k v) where
  typeConstruct _ = typeConstruct (Proxy @Map)


instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a ) => TSInstance a
instance {-# OVERLAPPING #-} TSInstance String where
  typeInstance _ = typeConstruct (Proxy @String) []
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSConstructName f ) => TSInstance (f a) where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ]
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSNamedInstance b
  , TSConstructName f ) => TSInstance (f a b) where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ,(lookupJSONKeyType (Proxy @b), typeInstance (Proxy @b))
    ]
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSNamedInstance b
  , TSNamedInstance c
  , TSConstructName f ) => TSInstance (f a b c) where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ,(lookupJSONKeyType (Proxy @b), typeInstance (Proxy @b))
    ,(lookupJSONKeyType (Proxy @c), typeInstance (Proxy @c))
    ]
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSNamedInstance b
  , TSNamedInstance c
  , TSNamedInstance d
  , TSConstructName f ) => TSInstance (f a b c d) where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ,(lookupJSONKeyType (Proxy @b), typeInstance (Proxy @b))
    ,(lookupJSONKeyType (Proxy @c), typeInstance (Proxy @c))
    ,(lookupJSONKeyType (Proxy @d), typeInstance (Proxy @d))
    ]
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSNamedInstance b
  , TSNamedInstance c
  , TSNamedInstance d
  , TSNamedInstance e
  , TSConstructName f ) => TSInstance (f a b c d e) where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ,(lookupJSONKeyType (Proxy @b), typeInstance (Proxy @b))
    ,(lookupJSONKeyType (Proxy @c), typeInstance (Proxy @c))
    ,(lookupJSONKeyType (Proxy @d), typeInstance (Proxy @d))
    ,(lookupJSONKeyType (Proxy @e), typeInstance (Proxy @e))
    ]
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSNamedInstance b
  , TSNamedInstance c
  , TSNamedInstance d
  , TSNamedInstance e
  , TSNamedInstance f'
  , TSConstructName f ) => TSInstance (f a b c d e f') where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ,(lookupJSONKeyType (Proxy @b), typeInstance (Proxy @b))
    ,(lookupJSONKeyType (Proxy @c), typeInstance (Proxy @c))
    ,(lookupJSONKeyType (Proxy @d), typeInstance (Proxy @d))
    ,(lookupJSONKeyType (Proxy @e), typeInstance (Proxy @e))
    ,(lookupJSONKeyType (Proxy @f'), typeInstance (Proxy @f'))
    ]
instance
  {-# OVERLAPPABLE #-}
  ( TSNamedInstance a
  , TSNamedInstance b
  , TSNamedInstance c
  , TSNamedInstance d
  , TSNamedInstance e
  , TSNamedInstance f'
  , TSNamedInstance g
  , TSConstructName f ) => TSInstance (f a b c d e f' g) where
  typeInstance _ = typeConstruct (Proxy @f)
    [(lookupJSONKeyType (Proxy @a), typeInstance (Proxy @a))
    ,(lookupJSONKeyType (Proxy @b), typeInstance (Proxy @b))
    ,(lookupJSONKeyType (Proxy @c), typeInstance (Proxy @c))
    ,(lookupJSONKeyType (Proxy @d), typeInstance (Proxy @d))
    ,(lookupJSONKeyType (Proxy @e), typeInstance (Proxy @e))
    ,(lookupJSONKeyType (Proxy @f'), typeInstance (Proxy @f'))
    ,(lookupJSONKeyType (Proxy @g), typeInstance (Proxy @g))
    ]
