{
  description = "Servant Aeson TypeScript";

  nixConfig = {
    extra-substituters = "https://horizon.cachix.org";
    extra-trusted-public-keys = "horizon.cachix.org-1:MeEEDRhRZTgv/FFGCv3479/dmJDfJ82G6kfUDxMSAw0=";
  };

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    horizon-platform.url = "git+https://gitlab.horizon-haskell.net/package-sets/horizon-platform";

    aeson-typescript.url = "git+https://gitlab.com/platonic/aeson-typescript";
    aeson-typescript.flake = false;

    servant-websockets = {
      url = "github:ursi/servant-websockets/TypedWebSocket";
      flake = false;
    };

    lint-utils = {
      url = "github:homotopic/lint-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs =
    inputs@
    { self
    , flake-utils
    , lint-utils
    , horizon-platform
    , aeson-typescript
    , servant-websockets
    , nixpkgs
    , ...
    }: with builtins; let
      inherit (inputs.nixpkgs) lib;
      onlyHaskell = fs2source
        (fs.union
          (onlyExts-fs [ "cabal" "hs" "project" ] ./.)
          ./LICENSE # horizon requires this file to build
        );
      fs = lib.fileset;
      fs2source = fs': path: fs.toSource { root = path; fileset = fs'; };
      onlyExts-fs = exts: fs.fileFilter (f: foldl' lib.or false (map f.hasExt exts));
      onlyExts = exts: path: fs2source (onlyExts-fs exts path) path;

    in
    flake-utils.lib.eachSystem [
      "x86_64-linux"
      "aarch64-linux"
      "x86_64-darwin"
      "aarch64-darwin"
    ]
      (system:
      let
        pkgs = import nixpkgs { inherit system; };
        hlib = pkgs.haskell.lib;

        legacyPackages =
          horizon-platform.legacyPackages.${system}.extend
            self.haskell-overlay.${system}.default;
      in
      {
        haskell-overlay.default = final: prev: {
          aeson-typescript = final.callCabal2nix "aeson-typescript" aeson-typescript { };
          servant-aeson-typescript = final.callCabal2nix "servant-aeson-typescript" (onlyHaskell ./.) { };
          servant-websockets = final.callCabal2nix "servant-websockets" servant-websockets { };
          regex-pcre = final.callHackage "regex-pcre" "0.95.0.0" { };
          webdriver = final.callCabal2nix "webdriver"
            (builtins.fetchGit
              {
                url = "https://github.com/haskell-webdriver/haskell-webdriver.git";
                ref = "main";
                rev = "b38d220375989cfe45968ba3abbd24e9f8edf9fc";
              })
            { };
          directory-tree = final.callHackage "directory-tree" "0.12.1" { };
          hspec-webdriver = final.callHackage "hspec-webdriver" "1.2.2" { };
        };
        packages.default = hlib.setBuildTarget legacyPackages.servant-aeson-typescript "lib:servant-aeson-typescript";
        devShells.default = legacyPackages.servant-aeson-typescript.env.overrideAttrs (attrs: {
          buildInputs = attrs.buildInputs ++ (with pkgs; [
            cabal-install
            haskellPackages.cabal-fmt
            legacyPackages.ghcid
            legacyPackages.hlint
            legacyPackages.haskell-language-server
            nixpkgs-fmt
            stylish-haskell

            nodePackages.typescript
            nodejs

            selenium-server-standalone
            chromedriver
            chromium
          ]);

          shellHook =
            let
              instructions = ''
                ${write-descriptions general-functions}
              '';

              general-functions = {
                format = {
                  description = "format code";
                  body = ''
                    nix fmt
                    stylish-haskell -ir .
                    cabal-fmt -i *.cabal
                  '';
                };

                shelp = {
                  description = "show this message";
                  body = "echo ${lib.escapeShellArg instructions}";
                };
              };


              write-set = set: f: concatStringsSep "\n" (lib.mapAttrsToList f set);

              write-descriptions = set: ''
                ${write-set set (n: v: "  ${n}\t${v.description}")}'';
            in
            ''
              ${write-set (general-functions)
                  (n: v: ''
                     ${n}() {
                       ${v.body}
                     }
                   '')
              }

              shelp
            '';
        });

        checks = let lu = lint-utils.linters.${system}; in {
          cabal-formatting = lu.cabal-fmt { src = onlyExts [ "cabal" ] ./.; };
          haskell-warnings = lu.werror { pkg = legacyPackages.servant-aeson-typescript; };
          haskell-formatting = lu.stylish-haskell {
            src = fs2source
              (fs.union (onlyExts-fs [ "hs" ] ./.) ./.stylish-haskell.yaml)
              ./.;
          };
          haskell-linting = lu.hlint {
            src = fs2source
              (fs.union (onlyExts-fs [ "hs" ] ./.) ./.hlint.yaml)
              ./.;
          };

          nix-formatting = lu.nixpkgs-fmt { src = onlyExts [ "nix" ] ./.; };

          tests = pkgs.runCommand "test.sh"
            {
              LANG = "en_US.UTF-8";
              buildInputs = with pkgs; [
                (hlib.justStaticExecutables legacyPackages.servant-aeson-typescript)
                nodePackages.typescript

                selenium-server-standalone
                chromedriver
                chromium
              ];
            } ''
            ln -s ${./test} test
            selenium-server &
            PID=$!
            tests || { kill $PID; exit 1; }
            kill $PID
            echo 0 > $out
          '';
        };

        formatter = pkgs.nixpkgs-fmt;
      });
}
