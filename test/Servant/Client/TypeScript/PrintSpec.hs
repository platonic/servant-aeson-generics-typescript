{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Servant.Client.TypeScript.TestAPIs should be imported qualified or with an explicit import list" #-}

module Servant.Client.TypeScript.PrintSpec
  ( main
  , spec
  ) where


import           Servant.API (type (:>))
import qualified Servant.Client.TypeScript as SCT (gen)
import           Servant.Client.TypeScript (GenAll, TSRouteName)
import           Servant.Client.TypeScript.TestAPIs
import           System.FilePath ((-<.>), (</>))
import           Test.Syd (Spec, describe, it, pureGoldenTextFile, sydTest)


main :: IO ()
main = sydTest spec

spec :: Spec
spec = do
  describe "Printing" printSpec

it' :: forall api. GenAll api => String -> String -> Spec
it' path msg = it (msg <> "|" <> path) $ ("test" </> "golden" </> path -<.> "ts") `pureGoldenTextFile` SCT.gen @api


printSpec :: Spec
printSpec = do
  it' @WebSocketAPI         "WebSocketAPI"         "Should be a trusting type casting WebSocket"
  it' @WebSocketSecureAPI   "WebSocketSecureAPI"   "Should allow for the WebSocket SEC header"
  it' @WebSocketAPIBinary   "WebSocketAPIBinary"   "Should fancy binary"
  it' @CaptureAPI           "CaptureAPI"           "Should interpolate variables into the url for Capture"
  it' @QueryAPI             "QueryAPI"             "Should interpolate variables into the url for Query"
  it' @(TSRouteName "thisIsANamedRoute" :> QueryAPI)
                      "QueryAPI_thisIsANamedRoute" "TSRouteNamed Should interpolate variables into the url for Query"
  it' @HeaderAPI            "HeaderAPI"            "Should interpolate variables into the url for Header"
  it' @FragmentAPI          "FragmentAPI"          "Should interpolate variables into the url for Fragment"
  it' @BodyAPI              "BodyAPI"              "Should interpolate variables into the url for Request Body"
  it' @BodyEnumMapAPI       "BodyEnumMapAPI"       "Maps with enumerative keys in Request Body"
  it' @BodyStringMapAPI     "BodyStringMapAPI"     "Maps with string like keys in Request Body"
  it' @BodyOddPrimsAPI      "BodyOddPrimsAPI"      "Things with custom typeConstructs all nested up"
  it' @MulipartAPI          "MulipartAPI"          "Should interpolate variables into the url for Multipart data"
  it' @MixedCaptureQueryAPI "MixedCaptureQueryAPI" "Should interpolate variables into the url for Mixed"
  it' @NoContentProper      "NoContentProper"      "No content is special and gets null hardwired in"
  it' @ResetContentProper   "ResetContentProper"   "Reset content is special and gets null hardwired in"
