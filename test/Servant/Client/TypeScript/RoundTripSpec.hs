{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wno-incomplete-record-updates #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Servant.Client.TypeScript.TestAPIs should be imported qualified or with an explicit import list" #-}

module Servant.Client.TypeScript.RoundTripSpec
  ( main
  , spec
  ) where

import           Control.Concurrent.Async (mapConcurrently_)
import           Control.Exception (SomeException, throwIO)
import qualified Control.Exception as Exception
import           Control.Monad (when)
import           Data.Aeson (FromJSON, ToJSON, encode)
import           Data.Aeson.TypeScript (derivingTypeScriptDefinition)
import qualified Data.Aeson.TypeScript as GGT
import           Data.ByteString (ByteString)
import           Data.Char (isAscii, isDigit, isLetter, toLower)
import qualified Data.Conduit.List as CL
import           Data.Kind (Type)
import           Data.List.Split (splitOn)
import           Data.Map as Map (lookup)
import           Data.Maybe (fromMaybe)
import           Data.Proxy (Proxy (Proxy))
import           Data.String (IsString)
import           Data.String.Interpolate (i)
import           Data.Time.Clock.POSIX (getPOSIXTime)
import           GHC.Generics (Generic)
import           Network.Wai.Handler.Warp (testWithApplication)
import           Servant
  ( GetNoContent
  , GetResetContent
  , NoContent (NoContent)
  , serveDirectoryFileServer
  )
import           Servant.API
  ( Capture
  , Description
  , Fragment
  , Get
  , Header
  , JSON
  , Post
  , QueryFlag
  , QueryParam
  , QueryParams
  , Raw
  , ReqBody
  , StdMethod (GET, POST)
  , Summary
  , Verb
  , type (:<|>) (..)
  , type (:>)
  )
import           Servant.API.WebSocketConduit
  ( WebSocketConduit
  , WebSocketConduitBinary
  )
import qualified Servant.Client.TypeScript as SCT (gen)
import           Servant.Client.TypeScript (GenAll, TSRouteName)
import           Servant.Client.TypeScript.TestAPIs
import qualified Servant.Multipart as MP
import           Servant.Server (HasServer, Server, serve)
import           System.Directory
  ( doesFileExist
  , getTemporaryDirectory
  , removeFile
  )
import           System.Exit (ExitCode (ExitFailure))
import           System.FilePath ((-<.>), (<.>), (</>))
import           System.Process (readProcess, readProcessWithExitCode)
import           System.Random (randomIO)
import           Test.QuickCheck (generate, resize, suchThat)
import           Test.QuickCheck.Arbitrary (Arbitrary (arbitrary))
import           Test.Syd (Spec, describe, it, parallel, shouldBe, sydTest)
import           Test.WebDriver
  ( Browser (chromeOptions)
  , asyncJS
  , chrome
  , closeSession
  , defaultConfig
  , openPage
  , runSession
  , useBrowser
  )


main :: IO ()
main = sydTest spec

spec :: Spec
spec = do
  describe "Round Trips" $ parallel roundTrips

toJSBool :: Bool -> String
toJSBool = fmap toLower . show

echoMaybe :: Applicative m => Maybe a -> m a
echoMaybe = \case
  Just b -> pure b
  _ -> error "Bool was not parsed from the frontend"

prop :: forall a. Arbitrary a => String -> Int -> (a -> IO ()) -> Spec
prop m runs p = it m $ do
  randos <- generate (sequence [ resize n (arbitrary @a) | n <- [1..runs] ])
  mapConcurrently_ p randos

roundTrips :: Spec
roundTrips = do
  prop "Should round trip for WebSocket health" parRuns \((age,toJSBool -> isAdmin',encode -> names) :: (Int,Bool,[AlphaNumAscii])) ->
   shouldRoundTrip @WebSocketAPI
    (CL.map id)
    ( "/foo/bar"
    , ""
    , [i|
    const msg = { names: #{names}, age: #{age}, isAdmin: #{isAdmin'} };
    res.receive(msg_ => {
      if(JSON.stringify(msg_.names) === JSON.stringify(msg.names)
         && msg_.age === msg.age
         && msg_.isAdmin === msg.isAdmin){
        return #{resolveSuccess}
      }
      return resolve('msg did not echo, got this instead: ' + JSON.stringify(msg_));
    })
    res.raw.onopen = () => res.send(msg);
    |])

  prop "Should round trip for Query" parRuns \((age,toJSBool -> isAdmin',encode -> names) :: (Int,Bool,[AlphaNumAscii])) ->
   shouldRoundTrip @QueryAPI
    (\ns g a -> User (AlphaNumAscii <$> a) ns <$> echoMaybe g)
    ( "/foo/bar/Zap?Frog%20Splat&wat&zazzy"
    , [i|#{age},#{isAdmin'},#{names}|]
    , [i|
    if(JSON.stringify(res.names) !== JSON.stringify(#{names})
       || res.age !== #{age}
       || res.isAdmin !== #{isAdmin'}){
      return resolve('responded with ' + JSON.stringify(res) + "\\n" + JSON.stringify(#{names}));
    }
    return #{resolveSuccess};|])

  prop "Should round trip for Capture" parRuns \((age,name,toJSBool -> isAdmin') :: (Int,AlphaNumAscii,Bool)) ->
   let names' = [encode name] in
   shouldRoundTrip @CaptureAPI
    (\ns a g -> pure $ User (pure $ AlphaNumAscii a) (Just ns) g)
    ( "/foo/bar/:Frog%20Splat/:wat/Zap/:zazzy"
    , [i|#{age},`#{name}`,#{isAdmin'}|]
    , [i|
    if(JSON.stringify(res.names) !== JSON.stringify(#{names'})
       || res.age !== #{age}
       || res.isAdmin !== #{isAdmin'}){
      return resolve('responded with ' + JSON.stringify(res) + "\\n" + JSON.stringify(#{names'}));
    }
    return #{resolveSuccess};|])

  it "Should round trip No Content Proper" $ shouldRoundTrip @NoContentProper
    (pure NoContent)
    ( "/foo/bar"
    , mempty
    , [i|
    if(res !== null){
      return resolve('responded with ' + JSON.stringify(res));
    }
    return #{resolveSuccess};|])

  it "Should round trip No Content Verbed" $ shouldRoundTrip @NoContentVerb
    (pure NoContent)
    ( "/foo/bar"
    , mempty
    , [i|
    if(res !== null){
      return resolve('responded with ' + JSON.stringify(res));
    }
    return #{resolveSuccess};|])

  it "Should round trip Reset Content" $ shouldRoundTrip @ResetContentProper
    (pure NoContent)
    ( "/foo/bar"
    , mempty
    , [i|
    if(res !== null){
      return resolve('responded with ' + JSON.stringify(res));
    }
    return #{resolveSuccess};|])

  prop "Should round trip for Header" parRuns \((age,name,toJSBool -> isAdmin') :: (Int,AlphaNumAscii,Bool)) ->
   let names = encode [name] in shouldRoundTrip @HeaderAPI
    (\ns a g -> User <$> (pure . AlphaNumAscii <$> echoMaybe a) <*> pure ns <*> echoMaybe g)
    ( "/foo/bar/Zap{Frog-Splat,wat,zazzy}"
    , [i|#{age},#{name},#{isAdmin'}|]
    , [i|
    if(JSON.stringify(res.names) !== `#{names}`
      || res.age !== #{age}
      || res.isAdmin !== #{isAdmin'}){
      return resolve('should respond with ' + JSON.stringify(res));
    }
    return #{resolveSuccess};|])

  prop "Should round trip for Fragment" parRuns \(frag :: AlphaNumAscii) -> shouldRoundTrip @FragmentAPI
    (pure $ unAlphaNumAscii frag)
    ( "/foo/bar#fragment"
    , mempty
    , [i|
    if(#{frag} !== res){
      return resolve('should respond with ' + JSON.stringify(res));
    }
    return #{resolveSuccess};|])

  prop "Should round trip for Request Body" parRuns \User{..} ->
    let isAdmin' = toJSBool isAdmin
        names' = encode names
        age' = fromMaybe 12 age
    in shouldRoundTrip @BodyAPI
    pure
    ( "/foo/bar(User)"
    , [i|{names:#{names},age:#{age'},isAdmin:#{isAdmin'}}|]
    , [i|
    if(JSON.stringify(res.names) !== `#{names'}`
       || res.age !== #{age'}
       || res.isAdmin !== #{isAdmin'}){
      return resolve("User was not as expected");
    }
    return #{resolveSuccess};|])

  it "Should round trip for Mixed" $ shouldRoundTrip @MixedCaptureQueryAPI
    (\_ _ _ _ _ _ _ -> pure $ User ["jack"] (Just 22) True)
    ( "/foo/bar/:Frog%20Splat/Zap/:zazzy?wat&rump&hu%20hu{bip,wip}"
    , "3,'wazzy','zammy',4,false,true,'grim'"
    , [i|
    if(res.names[0] === "jack"
       && res.age === 22
       && res.isAdmin){
      return #{resolveSuccess};
    }
    return resolve("User was not as expected");
    |])

  it "Should round trip for odd nested prims" $ shouldRoundTrip @BodyOddPrimsAPI
    (\_ -> pure $ User ["jack"] (Just 22) True)
    ( "/foo/bar(Either_MapEnumKeyString_MaybeInt_)"
    , "{ Left: [[\"A\",\"3\"],[\"B\",\"null\"]] }"
    , [i|
    if(res.names[0] === "jack"
       && res.age === 22
       && res.isAdmin){
      return #{resolveSuccess};
    }
    return resolve("User was not as expected");
    |])

  it "Should round trip for string maps" $ shouldRoundTrip @BodyStringMapAPI
    (pure . fromMaybe (error "key miss") . Map.lookup "What")
    ( "/foo/bar(MapStringUser)"
    , "{ What: { names: [\"jack\"], age: 4, isAdmin: true } }"
    , [i|
    if(res.names[0] === "jack"
       && res.age === 4
       && res.isAdmin){
      return #{resolveSuccess};
    }
    return resolve("User was not as expected");
    |])

srid :: String -> String
srid script = [i|<html>
<head>
  <script type="module" src="/#{script -<.> "js"}"></script>
</head>
<body></body>
</html>|]

successToken :: String
successToken = "success"

resolveSuccess :: String
resolveSuccess = [i|resolve("#{successToken}")|]

shouldRoundTrip :: forall (api :: Type).
  ( HasServer api '[]
  , GenAll api
  ) => Server api -> (String, String, String) -> IO ()
shouldRoundTrip app (path, args, test)
 = getTemporaryDirectory >>= \tmpDir ->
 testWithApplication (pure $ serve (Proxy @(api :<|> Raw)) (app :<|> serveDirectoryFileServer tmpDir)) $ \port -> do
  rand :: Int <- randomIO
  now <- getPOSIXTime

  let
    tsFilePath, localhost, ts :: String
    localhost = "http://127.0.0.1:" <> show port
    tsFilePath = show now <> show (rand * 1000000) <.> "ts"
    jsPath = tmpDir </> tsFilePath -<.> "js"
    ts = [i|
#{GGT.typeDefinition (Proxy @AlphaNumAscii)}
#{GGT.typeDefinition (Proxy @User)}
#{GGT.typeDefinition (Proxy @EnumKey)}
#{SCT.gen @api}
const testRaw = resolve => {
  API["#{path}"](#{args}).then(res => {
    #{test}
  });
};
window["test"] = resolve => {
  if(#{debugDelay}){
    setTimeout(() => testRaw(resolve), #{debugDelay});
  }else{
    testRaw(resolve);
  }
};

|]

    death = do
      removeFileIfExists $ tmpDir </> tsFilePath
      removeFileIfExists jsPath
      removeFileIfExists $ tmpDir </> tsFilePath <.> "html"

    handleFailure :: forall a. (ExitCode, String, String) -> String -> IO a -> IO a
    handleFailure res mes continue = case res of
      (ExitFailure ef, out, err) -> do
        death
        putStrLn "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        putStrLn $ addLineNumbers ts
        putStrLn "┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈"
        putStrLn out
        when (not (null err)) $ putStrLn err
        putStrLn "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        error $ mes <> " " <> show ef
      _ -> continue

  writeFile (tmpDir </> tsFilePath) ts
  writeFile (tmpDir </> tsFilePath <.> "html") (srid tsFilePath)

  tscres <- readProcessWithExitCode "tsc"
    [ tmpDir </> tsFilePath
    , "--lib", "ES2021,DOM"
    , "--module", "esnext"
    ] ""

  Exception.handle @SomeException
    (\e-> do
      putStrLn "------------------- TS File -------------------"
      tsFile <- readFile (tmpDir </> tsFilePath)
      putStrLn tsFile
      exists <- doesFileExist jsPath
      if exists then do
        putStrLn "------------------- JS File -------------------"
        jsFile <- readFile jsPath
        putStrLn jsFile
        putStrLn "------------------- HTML File -------------------"
        htmlFile <- readFile (tmpDir </> tsFilePath <.> "html")
        putStrLn htmlFile
        throwIO e
      else do
        putStrLn "JS file was not created or was created at a different path."
        putStrLn $ "jsPath: " <> jsPath
        ls <- readProcess "ls" [ tmpDir ] ""
        putStrLn "ls:"
        putStrLn ls
        putStrLn "Also there is the following error:"
        throwIO e
    )
    (handleFailure tscres "TSC exited with" do
      res <- runSession
        (useBrowser chrome { chromeOptions = if isHeadless then
          [ "--headless"
          , "--disable-gpu"
          ] else [] } defaultConfig)
        do
          openPage $ localhost </> tsFilePath <.> "html"
          res <- asyncJS [] [i|test(arguments[0])|]

          closeSession
          return res
      case res of
        Just x | x == successToken -> pure ()
        Just x                     -> death >> error x
        Nothing                    -> death >> error "TIMEOUT"
    )

removeFileIfExists :: FilePath -> IO ()
removeFileIfExists fp = do
  exists <- doesFileExist fp
  when exists $ removeFile fp

addLineNumbers :: String -> String
addLineNumbers ts =
  if showLineNumbers then foldMap (\(x,ln) ->
    let lnf = show (ln :: Int) in "\n " <> lnf <> replicate (4 - length lnf) ' ' <> "| " <> x)
      $ zip (splitOn "\n" ts) [1..]
  else ts
