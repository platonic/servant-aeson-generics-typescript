{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-incomplete-record-updates #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}

module Servant.Client.TypeScript.TestAPIs where

import           Control.Concurrent.Async (mapConcurrently_)
import           Control.Exception (SomeException, throwIO)
import qualified Control.Exception as Exception
import           Control.Monad (when)
import           Data.Aeson (FromJSON, FromJSONKey, ToJSON, ToJSONKey, encode)
import           Data.Aeson.TypeScript (derivingTypeScriptDefinition)
import qualified Data.Aeson.TypeScript as GGT
import           Data.ByteString (ByteString)
import           Data.Char (isAscii, isDigit, isLetter, toLower)
import qualified Data.Conduit.List as CL
import           Data.Kind (Type)
import           Data.List.Split (splitOn)
import           Data.Map (Map)
import           Data.Maybe (fromMaybe)
import           Data.Proxy (Proxy (Proxy))
import           Data.String (IsString)
import           Data.String.Interpolate (i)
import           Data.Time.Clock.POSIX (getPOSIXTime)
import           GHC.Generics (Generic)
import           Network.Wai.Handler.Warp (testWithApplication)
import           Servant
  ( GetNoContent
  , GetResetContent
  , NoContent (NoContent)
  , serveDirectoryFileServer
  )
import           Servant.API
  ( Capture
  , Description
  , Fragment
  , Get
  , Header
  , JSON
  , Post
  , QueryFlag
  , QueryParam
  , QueryParams
  , Raw
  , ReqBody
  , StdMethod (GET, POST)
  , Summary
  , Verb
  , type (:<|>) (..)
  , type (:>)
  )
import           Servant.API.WebSocketConduit
  ( WebSocketConduit
  , WebSocketConduitBinary
  )
import qualified Servant.Client.TypeScript as SCT (gen)
import           Servant.Client.TypeScript (GenAll, TSRouteName)
import qualified Servant.Multipart as MP
import           Servant.Server (HasServer, Server, serve)
import           System.Directory
  ( doesFileExist
  , getTemporaryDirectory
  , removeFile
  )
import           System.Exit (ExitCode (ExitFailure))
import           System.FilePath ((-<.>), (<.>), (</>))
import           System.Process (readProcess, readProcessWithExitCode)
import           System.Random (randomIO)
import           Test.QuickCheck (generate, resize, suchThat)
import           Test.QuickCheck.Arbitrary (Arbitrary (arbitrary))
import           Test.Syd (Spec, describe, it, parallel, shouldBe, sydTest)
import           Test.WebDriver
  ( Browser (chromeOptions)
  , asyncJS
  , chrome
  , closeSession
  , defaultConfig
  , openPage
  , runSession
  , useBrowser
  )

-- | TEST SETTINGS
showLineNumbers :: Bool
showLineNumbers = False
parRuns :: Int
parRuns = 1
isHeadless :: Bool
isHeadless = True
debugDelay :: Int
debugDelay = if isHeadless then 0 else 2000 -- milliseconds
--
--
newtype AlphaNumAscii = AlphaNumAscii { unAlphaNumAscii :: String }
  deriving newtype (Eq, FromJSON, IsString, Ord, Show, ToJSON)
  deriving stock (Generic)
$(derivingTypeScriptDefinition ''AlphaNumAscii)

type User :: Type
data User = User
  { names   :: [AlphaNumAscii]
  , age     :: Maybe Int
  , isAdmin :: Bool
  }
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (FromJSON, ToJSON)
$(derivingTypeScriptDefinition ''User)

instance Arbitrary AlphaNumAscii where
  arbitrary = AlphaNumAscii <$> arbitrary `suchThat` all (\x -> (isDigit x || isLetter x) && isAscii x)

instance Arbitrary User where
  arbitrary = User <$> arbitrary <*> arbitrary <*> arbitrary

type CaptureAPI :: Type
type CaptureAPI
  = Description "This is the description of this route" :>
  "foo" :> "bar"
  :> Capture "Frog Splat" Int
  :> Capture "wat" String
  :> "Zap"
  :> Capture "zazzy" Bool
  :> Get '[JSON] User

type NoContentProper :: Type
type NoContentProper
  = "foo" :> "bar"
  :> GetNoContent

type NoContentVerb :: Type
type NoContentVerb
  = "foo" :> "bar"
  :> Verb 'GET 204 '[JSON] NoContent

type ResetContentProper :: Type
type ResetContentProper
  = "foo" :> "bar"
  :> GetResetContent '[JSON] NoContent

type QueryAPI :: Type
type QueryAPI
  = Summary "This is the summary of this route"
  :> "foo" :> "bar"
  :> QueryParam "Frog Splat" Int
  :> QueryParam "wat" Bool
  :> "Zap"
  :> QueryParams "zazzy" String
  :> Post '[JSON] User

type HeaderAPI :: Type
type HeaderAPI
  = "foo" :> "bar"
  :> Header "Frog-Splat" Int
  :> Header "wat" String
  :> "Zap"
  :> Header "zazzy" Bool
  :> Post '[JSON] User

type BodyAPI :: Type
type BodyAPI
  = "foo" :> "bar"
  :> ReqBody '[JSON] User
  :> Post '[JSON] User

data EnumKey
  = A
  | B
  | C
  | D
  deriving stock (Eq, Generic, Ord, Show)
  deriving anyclass (FromJSON, FromJSONKey, ToJSON, ToJSONKey)
$(derivingTypeScriptDefinition ''EnumKey)

type BodyEnumMapAPI :: Type
type BodyEnumMapAPI
  = "foo" :> "bar"
  :> ReqBody '[JSON] (Map EnumKey User)
  :> Post '[JSON] User

type BodyStringMapAPI :: Type
type BodyStringMapAPI
  = "foo" :> "bar"
  :> ReqBody '[JSON] (Map String User)
  :> Post '[JSON] User

type BodyOddPrimsAPI :: Type
type BodyOddPrimsAPI
  = "foo" :> "bar"
  :> ReqBody '[JSON] (Either (Map EnumKey String) (Maybe Int))
  :> Post '[JSON] User

type MulipartAPI :: Type
type MulipartAPI
  = "foo" :> "bar"
  :> MP.MultipartForm MP.Tmp User
  :> Verb 'POST 201 '[JSON] User

type FragmentAPI :: Type
type FragmentAPI
  = "foo" :> "bar"
  :> Fragment String
  :> Post '[JSON] String

type MixedCaptureQueryAPI :: Type
type MixedCaptureQueryAPI
  = "foo" :> "bar"
  :> Capture "Frog Splat" Int
  :> QueryParam "wat" String
  :> Header "bip" String
  :> Header "wip" Int
  :> "Zap"
  :> QueryFlag "rump"
  :> Capture "zazzy" Bool
  :> QueryParam "hu hu" String
  :> Get '[JSON] User

type WebSocketAPI :: Type
type WebSocketAPI
  = "foo" :> "bar"
  :> WebSocketConduit User User

type WebSocketAPIBinary :: Type
type WebSocketAPIBinary
  = "foo" :> "bar"
  :> WebSocketConduitBinary User ByteString

type WebSocketSecureAPI :: Type
type WebSocketSecureAPI
  = "foo" :> "bar"
  :> Header "Dunno" String
  :> WebSocketConduit User User
