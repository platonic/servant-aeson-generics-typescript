export const API = {
  base: "",
  baseWS: "",
  /*
   * This is the description of this route
   */
  "/foo/bar/:Frog%20Splat/:wat/Zap/:zazzy": (() => {
  const urlBuilder = (Frog_Splat:number,wat:string,zazzy:boolean) => `${API.base}/foo/bar/${Frog_Splat}/${wat}/Zap/${zazzy}`;
  const f = async (Frog_Splat:number,wat:string,zazzy:boolean): Promise<User> => {
    const uri = urlBuilder(Frog_Splat,wat,zazzy);
    return fetch(uri, {
      method: "GET",
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 200
          ? (res.json() as Promise<User>)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};