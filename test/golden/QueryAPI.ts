export const API = {
  base: "",
  baseWS: "",
  // This is the summary of this route
  "/foo/bar/Zap?Frog%20Splat&wat&zazzy": (() => {
  const urlBuilder = (Frog_Splat:number,wat:boolean,zazzy:Array<string>) => `${API.base}/foo/bar/Zap?${Frog_Splat === null || Frog_Splat === undefined ? "" : `Frog%20Splat=${Frog_Splat}`}&${wat === null || wat === undefined ? "" : `wat=${wat}`}&${zazzy.reduceRight((acc,x) => x === null || x === undefined ? "" : "zazzy=" + x + (acc ? "&" + acc : ""), "")}`;
  const f = async (Frog_Splat:number,wat:boolean,zazzy:Array<string>): Promise<User> => {
    const uri = urlBuilder(Frog_Splat,wat,zazzy);
    return fetch(uri, {
      method: "POST",
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 200
          ? (res.json() as Promise<User>)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};