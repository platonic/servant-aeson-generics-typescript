export const API = {
  base: "",
  baseWS: "",
  "/foo/bar(MapEnumKeyUser)": (() => {
  const urlBuilder = () => `${API.base}/foo/bar`;
  const f = async (MapEnumKeyUser:[EnumKey,User][]): Promise<User> => {
    const uri = urlBuilder();
    return fetch(uri, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(MapEnumKeyUser),
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 200
          ? (res.json() as Promise<User>)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};