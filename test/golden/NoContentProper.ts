export const API = {
  base: "",
  baseWS: "",
  "/foo/bar": (() => {
  const urlBuilder = () => `${API.base}/foo/bar`;
  const f = async (): Promise<null> => {
    const uri = urlBuilder();
    return fetch(uri, {
      method: "GET",
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 204
          ? Promise.resolve(null)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};