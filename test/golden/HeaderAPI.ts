export const API = {
  base: "",
  baseWS: "",
  "/foo/bar/Zap{Frog-Splat,wat,zazzy}": (() => {
  const urlBuilder = () => `${API.base}/foo/bar/Zap`;
  const f = async (Frog_Splat:number,wat:string,zazzy:boolean): Promise<User> => {
    const uri = urlBuilder();
    return fetch(uri, {
      method: "POST",
      headers: {
        "Frog-Splat": "" + Frog_Splat,
        "wat": wat,
        "zazzy": "" + zazzy
      },
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 200
          ? (res.json() as Promise<User>)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};