export const API = {
  base: "",
  baseWS: "",
  "/foo/bar#fragment": (() => {
  const urlBuilder = () => `${API.base}/foo/bar`;
  const f = async (): Promise<string> => {
    const uri = urlBuilder();
    return fetch(uri, {
      method: "POST",
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 200
          ? (res.json() as Promise<string>)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};