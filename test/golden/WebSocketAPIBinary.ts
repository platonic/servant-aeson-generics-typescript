export const API = {
  base: "",
  baseWS: "",
  "/foo/bar": ():
    Promise<{ send : (input: User) => void
            , receive : (cb: (output: ArrayBuffer) => void) => void
            , raw : WebSocket
    }> => {
      if(!API.baseWS){
        const pr = window.location.protocol === "http:" ? "ws:" : "wss:";
        API.baseWS = `${pr}//${window.location.host}`;
        console.info(`You have not set API.baseWS, so it has been defaulted to ${API.baseWS}.
        Please note, that WebSocket's need to have absolute uri's including protocol.`);
      }
      const ws = new WebSocket(`${API.baseWS}/foo/bar`);
      return Promise.resolve({
        send: (input: User) => ws.send(JSON.stringify(input)),
        receive: (cb: ((output: ArrayBuffer) => void)) =>
          ws.onmessage = (message) => cb(message.data),
        raw: ws
      });
  }
};