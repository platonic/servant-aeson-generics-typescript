export const API = {
  base: "",
  baseWS: "",
  "/foo/bar(formData)": (() => {
  const urlBuilder = () => `${API.base}/foo/bar`;
  const f = async (formData:FormData): Promise<User> => {
    const uri = urlBuilder();
    return fetch(uri, {
      method: "POST",
      body: formData,
      redirect: 'manual'
    }).then(res => {
      const location = res.headers.get('Location');
      if (res.status === 401 && location) {
        window.location.replace(location);
        return Promise.reject(res);
      } else {
        return res.status === 201
          ? (res.json() as Promise<User>)
          : Promise.reject(res);
      }
    });
  };
  f.urlBuilder = urlBuilder;
  return f; })()
};